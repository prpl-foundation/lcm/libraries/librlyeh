/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/statvfs.h>
#include <stdint.h>
#include <inttypes.h>

#include <libocispec/image_spec_schema_image_manifest_schema.h>

#include <rlyeh/rlyeh.h>
#include <rlyeh_copy_priv.h>
#include <rlyeh_priv.h>
#include <debug/sahtrace.h>
#include <rlyeh_defines_priv.h>
#include <rlyeh_plugins_priv.h>
#include <curl/curl.h>

#include <lcm/lcm_assert.h>

#define ME "rlyeh_copy"

rlyeh_status_t rlyeh_size_check(const size_t expected_size, const size_t downloaded_size, const char* digest, char* err_msg) {
    if(expected_size != downloaded_size) {
        set_err_msg(err_msg, "Blob [%s] size mismatch: expected [%zu] downloaded [%zu]", digest, expected_size, downloaded_size);
        return RLYEH_ERROR_SIZE_CHECK;
    }
    return RLYEH_NO_ERROR;
}

// Returns true if there is an error.
static bool create_directory(const char* dirname, int mode) {
    struct stat s;
    int check;
    if((stat(dirname, &s) != 0) || !S_ISDIR(s.st_mode)) {
        check = mkdir(dirname, mode);
        if(check) {
            SAH_TRACEZ_ERROR(ME, "ERROR: Cannot create %s directory...", dirname);
            return true;
        }
    }
    return false;
}

// Recursive directory creation
static bool create_directories(const char* dirname, int mode) {
    char* path;
    char* subpath;
    char* copypath = strdup(dirname);
    bool status = false;

    path = copypath;
    while(!status && (subpath = strchr(path, '/')) != 0) {
        if(subpath != path) {
            /* Neither root nor double slash in path */
            *subpath = '\0';
            // fprintf(stderr, "trying to  create %s\n", copypath);
            status = create_directory(copypath, mode);
            *subpath = '/';
        }
        path = subpath + 1;
    }
    if(status == false) {
        // fprintf(stderr, "trying to  create %s\n", dirname);
        status = create_directory(dirname, mode);
    }
    free(copypath);
    return status;
}

rlyeh_status_t set_tmp_blob_filename(const char* digest,
                                     const rlyeh_copy_output_directories_t* output_dir,
                                     amxc_string_t* filename,
                                     char* err_msg) {
    rlyeh_status_t ret = RLYEH_NO_ERROR;
    char* sha_dir = NULL;
    char* blob_file = NULL;
    char* saveptr = NULL;
    amxc_string_t blob_dir;
    amxc_string_init(&blob_dir, 0);

    blob_file = digest_to_path(digest);

    amxc_string_setf(filename, "%s/%s.tmp", output_dir->blobs.buffer, blob_file);

    // Extract the SHA prefix to create a specific directory
    sha_dir = strtok_r(blob_file, "/", &saveptr);

    amxc_string_setf(&blob_dir, "%s/%s", output_dir->blobs.buffer, sha_dir);

    if(create_directories(blob_dir.buffer, 0755)) {
        set_err_msg(err_msg, "Failed to create %s", blob_dir.buffer);
        ret = RLYEH_ERROR_DISK_SPACE;
    }

    amxc_string_clean(&blob_dir);
    free(blob_file);

    return ret;
}

static rlyeh_status_t rlyeh_verify_checksum(const char* tmp_filename, const char* digest, char* err_msg) {
    // TODO: fix getting digest and type of digest to hash
    rlyeh_status_t ret = RLYEH_NO_ERROR;
    char* blob_hash = NULL;
    char* cpy_digest = strdup(digest);
    const char* hash_algo = cpy_digest;
    char* digest_itself = cpy_digest;
    int retval = 0;

    while(digest_itself[0] != '\0') {
        if(digest_itself[0] == ':') {
            digest_itself[0] = '\0';
            digest_itself++;
            break;
        }
        digest_itself++;
    }

    if(digest_itself[0] == '\0') {
        set_err_msg(err_msg, "Broken digest 'hash_algo:digest_itself' [%s]: %s", tmp_filename, digest);
        ret = RLYEH_ERROR_INVALID_CONTENT;
        goto exit;
    }

    retval = rlyeh_hash_blob(tmp_filename, hash_algo, &blob_hash);
    if(retval < 1) {
        set_err_msg(err_msg, "Cannot hash file [%s]: %s", tmp_filename, hash_algo);
        ret = RLYEH_ERROR_INVALID_CONTENT;
        goto exit;
    }

    if(strcmp(digest_itself, blob_hash)) {
        set_err_msg(err_msg, "Incorrect hash for file [%s] expected: (%s) '%s' got '%s'", tmp_filename, hash_algo, digest_itself, blob_hash);
        ret = RLYEH_ERROR_INVALID_CONTENT;
    }

exit:
    free(blob_hash);
    free(cpy_digest);
    return ret;
}

static rlyeh_status_t retrieve_manifest(const rlyeh_image_parameters_t* parameters,
                                        amxc_string_t* file_retrieved,
                                        curl_off_t* size,
                                        char* err_msg) {
    rlyeh_status_t ret = RLYEH_NO_ERROR;
    amxc_string_t url;
    rlyeh_curl_parameters_t manifest_parameters = {
        .username = parameters->username,
        .password = parameters->password,
        .token = parameters->token,
        .type = FILE_TYPE_MANIFEST,
        .auth_type = parameters->auth_type,
        .cv = parameters->cv
    };
    char* manifestpath = tempnam(RLYEH_TMP_DIR, "manif");
    amxc_string_init(&url, 0);
    amxc_string_setf(&url, "https://%s%s%s/manifests/%s", parameters->server, parameters->server_url_append, parameters->image_name, parameters->version);
    amxc_string_set(file_retrieved, manifestpath);

    FILE* fd = fopen("/dev/null", "w");
    ret = rlyeh_curl_head(&manifest_parameters, &url, (void*) fd, size, err_msg);
    fclose(fd);
    if(ret) {
        goto exit;
    }

    // TODO : check disk space before download content
    // struct statvfs st;
    // statvfs(file_retrieved->buffer, &st);
    // unsigned long free_space = st.f_bfree * st.f_frsize;
    // fprintf(stderr, "manifest size : %ld\n", *size);
    // fprintf(stderr, "free space : %ld\n", free_space);
    // fprintf(stderr, "f_frsize : %ld\n", st.f_frsize);

    fd = fopen(manifestpath, "w");
    ret = rlyeh_curl_download_content(&manifest_parameters, &url, (void*) fd, size, err_msg);
    fclose(fd);
    if(ret) {
        remove(manifestpath);
        goto exit;
    }

exit:
    free(manifestpath);
    amxc_string_clean(&url);
    return ret;
}

static rlyeh_status_t retrieve_config(const rlyeh_image_parameters_t* parameters,
                                      const char* digest,
                                      const rlyeh_copy_output_directories_t* output_dir,
                                      const int64_t expected_size,
                                      char* err_msg) {
    rlyeh_status_t ret = RLYEH_NO_ERROR;
    amxc_string_t tmp_filename;
    amxc_string_t filename;
    amxc_string_t url;
    curl_off_t size;
    rlyeh_curl_parameters_t config_parameters = {
        .username = parameters->username,
        .password = parameters->password,
        .token = parameters->token,
        .type = FILE_TYPE_CONFIG,
        .auth_type = parameters->auth_type,
        .cv = parameters->cv
    };
    amxc_string_init(&tmp_filename, 0);
    amxc_string_init(&filename, 0);

    amxc_string_init(&url, 0);
    amxc_string_setf(&url, "https://%s%s%s/blobs/%s", parameters->server, parameters->server_url_append, parameters->image_name, digest);

    ret = set_tmp_blob_filename(digest, output_dir, &tmp_filename, err_msg);
    ASSERT_SUCCESS_NL(ret, goto exit);

    // Shrink to check if the blob already exists
    amxc_string_copy(&filename, &tmp_filename);
    amxc_string_shrink(&filename, 4);

    FILE* fd = fopen(tmp_filename.buffer, "w");
    ret = rlyeh_curl_download_content(&config_parameters, &url, (void*) fd, &size, err_msg);
    fclose(fd);
    if((ret == RLYEH_NO_ERROR) && (expected_size > 0)) {
        ret = rlyeh_size_check(expected_size, (int64_t) size, digest, err_msg);
    }
    if(ret == RLYEH_NO_ERROR) {
        ret = rlyeh_verify_checksum(tmp_filename.buffer, digest, err_msg);
    }
    if(ret == RLYEH_NO_ERROR) {
        if(rlyeh_move_file(tmp_filename.buffer, filename.buffer) != 0) {
            set_err_msg(err_msg, "Couldnt move config from %s to %s", tmp_filename.buffer, filename.buffer);
            ret = RLYEH_ERROR_UNKNOWN_ERROR;
        }
    }

    if(ret) {
        remove(tmp_filename.buffer);
    }
exit:
    amxc_string_clean(&tmp_filename);
    amxc_string_clean(&filename);
    amxc_string_clean(&url);
    return ret;
}

static rlyeh_status_t retrieve_layer(const rlyeh_image_parameters_t* parameters,
                                     const char* digest,
                                     const rlyeh_copy_output_directories_t* output_dir,
                                     const int64_t expected_size,
                                     char* err_msg) {
    rlyeh_status_t ret = RLYEH_NO_ERROR;
    amxc_string_t tmp_filename;
    amxc_string_t filename;
    amxc_string_t url;
    curl_off_t size;
    rlyeh_curl_parameters_t layer_parameters = {
        .username = parameters->username,
        .password = parameters->password,
        .token = parameters->token,
        .type = FILE_TYPE_BLOB,
        .auth_type = parameters->auth_type
    };

    amxc_string_init(&tmp_filename, 0);
    amxc_string_init(&filename, 0);

    amxc_string_init(&url, 0);
    amxc_string_setf(&url, "https://%s%s%s/blobs/%s", parameters->server, parameters->server_url_append, parameters->image_name, digest);

    ret = set_tmp_blob_filename(digest, output_dir, &tmp_filename, err_msg);
    ASSERT_SUCCESS_NL(ret, goto exit);

    // Shrink to check if the blob already exists
    amxc_string_copy(&filename, &tmp_filename);
    amxc_string_shrink(&filename, 4);

    FILE* fd = fopen(tmp_filename.buffer, "w");
    ret = rlyeh_curl_download_content(&layer_parameters, &url, (void*) fd, &size, err_msg);
    fclose(fd);
    if((ret == RLYEH_NO_ERROR) && (expected_size > 0)) {
        ret = rlyeh_size_check(expected_size, (int64_t) size, digest, err_msg);
    }
    if(ret == RLYEH_NO_ERROR) {
        ret = rlyeh_verify_checksum(tmp_filename.buffer, digest, err_msg);
    }
    if(ret == RLYEH_NO_ERROR) {
        if(rlyeh_move_file(tmp_filename.buffer, filename.buffer) != 0) {
            set_err_msg(err_msg, "Couldnt move blob from %s to %s", tmp_filename.buffer, filename.buffer);
            ret = RLYEH_ERROR_UNKNOWN_ERROR;
        }
    }
    if(ret) {
        remove(tmp_filename.buffer);
    }

exit:
    amxc_string_clean(&tmp_filename);
    amxc_string_clean(&filename);
    amxc_string_clean(&url);

    return ret;
}

static rlyeh_status_t download_config(const rlyeh_image_parameters_t* parameters,
                                      const image_spec_schema_image_manifest_schema* image_manifest,
                                      const rlyeh_copy_output_directories_t* output_dir,
                                      char* err_msg) {
    int64_t expected_size = 0;
    if(image_manifest->config->size_present) {
        expected_size = image_manifest->config->size;
    }
    return retrieve_config(parameters, image_manifest->config->digest, output_dir, expected_size, err_msg);
}

static int layer_exists_in_blob_stores(const char* digest, const rlyeh_copy_output_directories_t* output_dir) {
    int ret = 0;
    amxc_string_t layer_in_rw_blob_store;
    char* digest_path = digest_to_path(digest);
    const char* rw_blob_store = output_dir->blobs.buffer;
    const char* ro_blob_store = output_dir->ro_blobs.buffer;
    amxc_string_init(&layer_in_rw_blob_store, 256);
    amxc_string_setf(&layer_in_rw_blob_store, "%s/%s", rw_blob_store, digest_path);

    if(file_exists(layer_in_rw_blob_store.buffer)) {
        ret = 1;
        SAH_TRACEZ_NOTICE(ME, "Found layer in RW Blob store: '%s'", layer_in_rw_blob_store.buffer);
    } else if(ro_blob_store) {
        amxc_string_t layer_in_ro_blob_store;
        amxc_string_init(&layer_in_ro_blob_store, 256);
        amxc_string_setf(&layer_in_ro_blob_store, "%s/%s", ro_blob_store, digest_path);

        if(file_exists(layer_in_ro_blob_store.buffer)) {
            SAH_TRACEZ_NOTICE(ME, "Found layer in RO Blob store: Symlinking: '%s' --> '%s'", layer_in_rw_blob_store.buffer, layer_in_ro_blob_store.buffer);
            if(symlink(layer_in_ro_blob_store.buffer, layer_in_rw_blob_store.buffer) == 0) {
                ret = 2;
            } else {
                SAH_TRACEZ_ERROR(ME, "Cannot symlink (%d): '%s'", errno, strerror(errno));
            }
        }

        amxc_string_clean(&layer_in_ro_blob_store);
    }

    amxc_string_clean(&layer_in_rw_blob_store);
    free(digest_path);
    return ret;
}

static rlyeh_status_t download_layers(const rlyeh_image_parameters_t* parameters,
                                      const image_spec_schema_image_manifest_schema* image_manifest,
                                      const rlyeh_copy_output_directories_t* output_dir,
                                      char* err_msg) {
    rlyeh_status_t ret = RLYEH_NO_ERROR;
    if(image_manifest->layers_len != 0) {
        for(size_t i = 0; i < image_manifest->layers_len; i++) {
            const char* digest = image_manifest->layers[i]->digest;
            if(layer_exists_in_blob_stores(digest, output_dir) == 0) {
                int64_t expected_size = 0;
                if(image_manifest->layers[i]->size_present) {
                    expected_size = image_manifest->layers[i]->size;
                }
                SAH_TRACEZ_NOTICE(ME, "Pulling layer '%s'", digest);
                ret = retrieve_layer(parameters, digest, output_dir, expected_size, err_msg);
            }
        }
    } else {
        set_err_msg(err_msg, "No Layers in the manifest!!");
        SAH_TRACEZ_ERROR(ME, "No Layers in the manifest!!");
        ret = RLYEH_ERROR_UNKNOWN_ERROR;
    }
    return ret;
}

static rlyeh_status_t rename_manifest(const amxc_string_t* manifest_filename, const rlyeh_copy_output_directories_t* output_dir, const char* digest, char* err_msg) {
    rlyeh_status_t ret = RLYEH_NO_ERROR;
    amxc_string_t blob_dir;
    amxc_string_t new_name;
    amxc_string_init(&blob_dir, 0);
    amxc_string_init(&new_name, 0);

    // Create blob directory
    amxc_string_setf(&blob_dir, "%s/sha256", output_dir->blobs.buffer);
    create_directories(blob_dir.buffer, 0755);

    // Rename manifest.json with the digest contained in the index
    amxc_string_setf(&new_name, "%s/%s", blob_dir.buffer, digest);
    if(rlyeh_move_file(manifest_filename->buffer, new_name.buffer) != 0) {
        ret = RLYEH_ERROR_UNKNOWN_ERROR;
        set_err_msg(err_msg, "Couldnt move manifest from %s to %s", manifest_filename->buffer, new_name.buffer);
    }

    amxc_string_clean(&new_name);
    amxc_string_clean(&blob_dir);
    return ret;
}

// This function download the manifest and the blobs (config and layers)
static rlyeh_status_t download_image_files(rlyeh_image_parameters_t* src_parameters,
                                           rlyeh_imagespec_manifest_element_t* manifest_ele,
                                           const rlyeh_copy_output_directories_t* output_dir,
                                           char* err_msg) {
    image_spec_schema_image_manifest_schema* image_manifest = NULL;
    parser_error err = NULL;
    rlyeh_status_t ret = RLYEH_NO_ERROR;
    curl_off_t size = 0;
    amxc_string_t manifest_filename;
    rlyeh_signature_data_t sigdata;
    amxc_string_init(&manifest_filename, 0);
    rlyeh_signature_data_init(&sigdata);
    sigdata.image_name = strdup(src_parameters->image_name);
    sigdata.transport = strdup(src_parameters->transport);
    sigdata.server = strdup(src_parameters->server);
    sigdata.version = strdup(src_parameters->version);
    if(src_parameters->signature_url_overload) {
        sigdata.signature_url_overload = strdup(src_parameters->signature_url_overload);
    }

    // Retrieve manifest.json
    ret = retrieve_manifest(src_parameters, &manifest_filename, &size, err_msg);
    ASSERT_SUCCESS_NL(ret, goto exit);

    image_manifest
        = image_spec_schema_image_manifest_schema_parse_file(manifest_filename.buffer,
                                                             NULL,
                                                             &err);
    if(check_manifest_validity(image_manifest, err)) {
        set_err_msg(err_msg, "Image manifest is invalid [%s]: %s", manifest_filename.buffer, err);
        ret = RLYEH_ERROR_INVALID_CONTENT;
        goto exit;
    }

    // Hash manifest
    manifest_ele->manifest_size = rlyeh_hash_blob(manifest_filename.buffer, "sha256", &(manifest_ele->digest));
    if(manifest_ele->manifest_size == 0) {
        set_err_msg(err_msg, "Cannot hash the manifest [%s] (sha256)", manifest_filename.buffer);
        ret = RLYEH_ERROR_INVALID_CONTENT;
        goto exit;
    }

    ret = rlyeh_size_check(manifest_ele->manifest_size, (int64_t) size, manifest_ele->digest, err_msg);
    if(ret) {
        SAH_TRACEZ_ERROR(ME, "Manifest downloaded size doesnt match the hashed size");
        goto exit;
    }

    // Verify image signature
    rlyeh_policy_ctx_t policy = rlyeh_read_policy("/etc/amx/librlyeh/policy.json", sigdata.transport, sigdata.server);
    if(src_parameters->sv && ((policy == GPG_KEY_SIGNATURE) || (src_parameters->signature_url_overload != NULL))) {
        sigdata.manifest_hash = strdup(manifest_ele->digest);
        if(plugin_image_authentication(&sigdata)) {
            set_err_msg(err_msg, "Image signature is invalid %s [%s]", manifest_ele->digest, manifest_filename.buffer);
            ret = RLYEH_ERROR_INVALID_SIGNATURE;
            goto exit;
        }
        SAH_TRACEZ_NOTICE(ME, "Valid signature: %s [%s]", manifest_ele->digest, manifest_filename.buffer);
    } else {
        SAH_TRACEZ_NOTICE(ME, "Not checking signature due to policy: (%s) (%d) %s [%s]", (src_parameters->sv ? "true" : "false"), policy, manifest_ele->digest, manifest_filename.buffer);
    }

    fill_imagespec_manifest_element_data_from_manifest_schema(image_manifest, manifest_ele);

    ret = rename_manifest(&manifest_filename, output_dir, manifest_ele->digest, err_msg);
    if(ret) {
        set_err_msg(err_msg, "Manifest renaming/moving failed [%s] %d", manifest_filename.buffer, ret);
        goto exit;
    }

    ret = download_config(src_parameters, image_manifest, output_dir, err_msg);
    ASSERT_SUCCESS_NL(ret, goto exit);

    ret = download_layers(src_parameters, image_manifest, output_dir, err_msg);
    ASSERT_SUCCESS_NL(ret, goto exit);

exit:
    free(err);
    rlyeh_signature_data_clean(&sigdata);
    if(ret) {
        remove(manifest_filename.buffer);
    }
    amxc_string_clean(&manifest_filename);
    if(image_manifest) {
        free_image_spec_schema_image_manifest_schema(image_manifest);
        image_manifest = NULL;
    }
    return ret;
}

static rlyeh_status_t create_output_directories(const rlyeh_copy_data_t* data,
                                                const rlyeh_image_parameters_t* image_parameters,
                                                rlyeh_copy_output_directories_t* output_dir,
                                                char* err_msg) {
    // Create image directory and default blob directory
    amxc_string_set(&output_dir->image, image_parameters->image_dir);

    if(create_directories(output_dir->image.buffer, 0755)) {
        set_err_msg(err_msg, "Could not create dir %s", output_dir->image.buffer);
        return RLYEH_ERROR_DISK_SPACE;
    }

    // Create blobs directory
    if(data->dest_shared_blob_dir) {
        amxc_string_reset(&output_dir->blobs);
        amxc_string_set(&output_dir->blobs, data->dest_shared_blob_dir);
    } else {
        amxc_string_copy(&output_dir->blobs, &output_dir->image);
        amxc_string_append(&output_dir->blobs, "/blobs", strlen("/blobs"));
    }

    // Add RO blobs location
    if(data->ro_shared_blob_dir) {
        SAH_TRACEZ_NOTICE(ME, "Using '%s' for RO Blob store directory", data->ro_shared_blob_dir);
        if(dir_exists(data->ro_shared_blob_dir)) {
            amxc_string_reset(&output_dir->ro_blobs);
            amxc_string_set(&output_dir->ro_blobs, data->ro_shared_blob_dir);
        } else {
            SAH_TRACEZ_ERROR(ME, "RO Blob store directory doesn't exist, let's continue without");
        }
    }

    if(create_directories(output_dir->blobs.buffer, 0755)) {
        set_err_msg(err_msg, "Could not create dir %s", output_dir->blobs.buffer);
        return RLYEH_ERROR_DISK_SPACE;
    }

    return RLYEH_NO_ERROR;
}

static rlyeh_status_t print_parse_input(const rlyeh_copy_data_t* input_data, rlyeh_image_parameters_t* src, rlyeh_image_parameters_t* dest, char* err_msg) {
    int retval;

    if(input_data->source == NULL) {
        set_err_msg(err_msg, "Missing rlyeh_copy_data_t source");
        return RLYEH_ERROR_INVALID_ARGUMENTS;
    }

    if(input_data->destination == NULL) {
        set_err_msg(err_msg, "Missing rlyeh_copy_data_t destination");
        return RLYEH_ERROR_INVALID_ARGUMENTS;
    }

    SAH_TRACEZ_INFO(ME, "SRC: %s DEST: %s", input_data->source, input_data->destination);

    if(input_data->dest_shared_blob_dir) {
        SAH_TRACEZ_INFO(ME, "DEST-SHARED-BLOB-DIR: %s", input_data->dest_shared_blob_dir);
    } else {
        SAH_TRACEZ_WARNING(ME, "No DEST-SHARED-BLOB-DIR set");
    }

    if(input_data->username && input_data->password) {
        src->username = strdup(input_data->username);
        src->password = strdup(input_data->password);
        SAH_TRACEZ_INFO(ME, "Username: (%s) and Password: (%s)", src->username, src->password);
    }

    // Parse arguments
    retval = rlyeh_parse_uri(input_data->source, src);
    if(retval) {
        set_err_msg(err_msg, "Couldnt parse source %s (%d)", input_data->source, retval);
        return RLYEH_ERROR_INVALID_ARGUMENTS;
    }

    retval = rlyeh_parse_uri(input_data->destination, dest);
    if(retval) {
        set_err_msg(err_msg, "Couldnt parse destination %s (%d)", input_data->destination, retval);
        return RLYEH_ERROR_INVALID_ARGUMENTS;
    }

    src->sv = input_data->sv;
    src->cv = input_data->cv;
    if(input_data->signature_url_overload) {
        SAH_TRACEZ_INFO(ME, "Setting signature_url_overload (%s)", input_data->signature_url_overload);
        src->signature_url_overload = strdup(input_data->signature_url_overload);
    }

    // Dest must be OCI
    if(strcmp(dest->transport, "oci")) {
        set_err_msg(err_msg, "dest MUST be 'oci' [%s]", dest->transport);
        return RLYEH_ERROR_INVALID_ARGUMENTS;
    }

    if(strcmp(src->transport, "docker")) {
        set_err_msg(err_msg, "source MUST be 'docker' [%s]", src->transport);
        return RLYEH_ERROR_INVALID_ARGUMENTS;
    }

    // to adhere to the spec - might need evolutions in the future
    src->server_url_append = strdup("/v2/");

    // Set default server if not found in the uri
    if((src->server == NULL) || (src->server[0] == '\0')) {
        SAH_TRACEZ_INFO(ME, "Getting server from the configuration file");
        rlyeh_get_server_from_file(src);
    }

    if((src->username == NULL) || (src->password == NULL)) {
        if(rlyeh_get_creds_from_file(src, src->server) == 0) {
            SAH_TRACEZ_INFO(ME, "Got creds from file for server %s", src->server);
        }
    }

    if((src->server == NULL) || (src->server[0] == '\0')) {
        set_err_msg(err_msg, "Cannot determine server: %s", input_data->source);
        return RLYEH_ERROR_INVALID_ARGUMENTS;
    }

    SAH_TRACEZ_INFO(ME, "src server: (%s) and src server_url_append: (%s)", src->server, src->server_url_append);
    SAH_TRACEZ_INFO(ME, "dest image_dir: (%s)", dest->image_dir);
    return RLYEH_NO_ERROR;
}

static rlyeh_status_t download_token(const amxc_string_t* url, rlyeh_image_parameters_t* data, char* err_msg, bool cv) {
    rlyeh_status_t status = RLYEH_NO_ERROR;
    amxc_var_t* config_data = NULL;
    char* rlyeh_temp_token_template = tempnam(RLYEH_TMP_DIR, "token");
    FILE* token = fopen(rlyeh_temp_token_template, "w");
    status = rlyeh_curl_get(url, data, token, err_msg, cv);
    fclose(token);
    if(status) {
        SAH_TRACEZ_ERROR(ME, "Failed getting %s: (%d) %s", rlyeh_temp_token_template, status, (err_msg ? err_msg : "????"));
        goto exit;
    }

    config_data = read_config(rlyeh_temp_token_template);
    if(config_data) {
        amxc_var_t* var_token = GET_ARG(config_data, "token");
        const char* str_token = amxc_var_constcast(cstring_t, var_token);
        if(str_token) {
            SAH_TRACEZ_INFO(ME, "Token: [ %s ]'", str_token);
            data->token = strdup(str_token);
        }
        amxc_var_delete(&var_token);
        amxc_var_delete(&config_data);
    }

exit:
    remove(rlyeh_temp_token_template);
    free(rlyeh_temp_token_template);
    return status;
}

void get_authenticate_element(const char* buf, char** realm, char** service, char** scope) {
    char* bufcpy = strdup(buf);
    char* saveptr_line = NULL;
    char* saveptr_word = NULL;
    char* line = strtok_r(bufcpy, "\r\n", &saveptr_line);
    do {
        if(strncasecmp(line, "www-authenticate", strlen("www-authenticate")) == 0) {
            // fprintf(stderr, "line(%s)\n", line);
            char* word = strtok_r(line, ", ", &saveptr_word);
            do {
                // fprintf(stderr, "word(%s)\n", word);
                if(strncasecmp(word, "realm", strlen("realm")) == 0) {
                    if(!(*realm)) {
                        *realm = (char*) calloc(1, strlen(word) - 7);
                        strncpy(*realm, word + 7, strlen(word) - 8);
                        // fprintf(stderr, "[%s]\n", *realm);
                    }
                }
                if(strncasecmp(word, "service", strlen("service")) == 0) {
                    if(!(*service)) {
                        *service = (char*) calloc(1, strlen(word) - 9);
                        strncpy(*service, word + 9, strlen(word) - 10);
                        // fprintf(stderr, "[%s]\n", *service);
                    }
                }
                if(strncasecmp(word, "scope", strlen("scope")) == 0) {
                    if(!(*scope)) {
                        *scope = (char*) calloc(1, strlen(word) - 7);
                        strncpy(*scope, word + 7, strlen(word) - 8);
                        // fprintf(stderr, "[%s]\n", *scope);
                    }
                }
            } while((word = strtok_r(NULL, ", ", &saveptr_word)));
            break;
        }
    } while((line = strtok_r(NULL, "\r\n", &saveptr_line)));

    free(bufcpy);
}

static rlyeh_status_t retrieve_token(const char* buf, rlyeh_image_parameters_t* src, char* err_msg) {
    rlyeh_status_t status = RLYEH_NO_ERROR;
    char* realm = NULL;
    char* service = NULL;
    char* scope = NULL;
    amxc_string_t url;
    amxc_string_init(&url, 0);

    get_authenticate_element(buf, &realm, &service, &scope);
    // fprintf(stderr, "[%s][%s][%s]\n", realm, service, scope);

    if(realm && service && scope) {
        amxc_string_setf(&url, "%s?scope=%s&service=%s", realm, scope, service);
    } else {
        set_err_msg(err_msg, "Can't retrieve token for authentication");
        status = RLYEH_ERROR_UNKNOWN_ERROR;
        goto exit;
    }

    status = download_token(&url, src, err_msg, src->cv);

exit:
    amxc_string_clean(&url);
    free(realm);
    free(service);
    free(scope);
    return status;
}

static rlyeh_status_t rlyeh_get_authentication_data(rlyeh_image_parameters_t* src,
                                                    char* err_msg) {
    rlyeh_status_t ret = RLYEH_NO_ERROR;
    long auth;
    char* buf = NULL;
    amxc_string_t url;
    amxc_string_init(&url, 0);
    amxc_string_setf(&url, "https://%s%s%s/manifests/%s", src->server, src->server_url_append, src->image_name, src->version);

    ret = rlyeh_curl_get_httpauthentication(&url, (void*) &buf, &auth, err_msg, src->cv);
    ASSERT_SUCCESS_NL(ret, goto exit);

    if(auth & CURLAUTH_BASIC) {
        src->auth_type = BASIC_AUTHENTICATION;
    } else if(auth & CURLAUTH_BEARER) {
        src->auth_type = BEARER_AUTHENTICATION;
        ret = retrieve_token(buf, src, err_msg);
    } else {
        src->auth_type = NO_AUTHENTICATION;
    }

exit:
    amxc_string_clean(&url);
    free(buf);

    return ret;
}

static void replace_in_buffer(char* str, const char* oldWord, const char* newWord) {
    char* pos, temp[512];
    int index = 0;
    int owlen = strlen(oldWord);

    if(!strcmp(oldWord, newWord)) {
        return;
    }

    while((pos = strstr(str, oldWord)) != NULL) {
        strcpy(temp, str);
        index = pos - str;
        str[index] = '\0';
        strcat(str, newWord);
        strcat(str, temp + index + owlen);
    }
}

static int patch_mediatype(const rlyeh_copy_output_directories_t* output_dir, const char* version, const char* duid) {
    int ret = -1;
    amxc_string_t fname_index;
    amxc_string_t fname_manifest;
    amxc_string_t fname_tmp;
    amxc_string_init(&fname_index, 0);
    amxc_string_init(&fname_manifest, 0);
    amxc_string_init(&fname_tmp, 0);

    amxc_string_setf(&fname_index, "%s/index.json", output_dir->image.buffer);
    // fprintf(stderr, "index.json [%s]\n", fname_index.buffer);

    image_spec_schema_image_index_schema* index_schema = rlyeh_parse_image_spec_schema(fname_index.buffer);
    if(!index_schema) {
        SAH_TRACEZ_ERROR(ME, "Index file does not exist [%s]", fname_index.buffer);
        goto exit;
    }

    build_manifest_filename_from_index(index_schema, version, duid, output_dir->blobs.buffer, &fname_manifest);
    free_image_spec_schema_image_index_schema(index_schema);

    // fprintf(stderr, "manifest [%s]\n", fname_manifest.buffer);
    if(!file_exists(fname_manifest.buffer)) {
        SAH_TRACEZ_ERROR(ME, "Manifest not found to patch mediatype %s", fname_manifest.buffer);
        goto exit;
    }

    // Store the patched manifest in <manifest_name>.tmp
    amxc_string_copy(&fname_tmp, &fname_manifest);
    amxc_string_append(&fname_tmp, ".tmp", strlen(".tmp"));

    // Replace mediatype
    FILE* old = fopen(fname_manifest.buffer, "r");
    FILE* renamed = fopen(fname_tmp.buffer, "w");
    char buffer[2048];
    while((fgets(buffer, sizeof(buffer), old)) != NULL) {
        replace_in_buffer(buffer, "vnd.docker.distribution.manifest.v2", "vnd.oci.image.manifest.v1");
        replace_in_buffer(buffer, "vnd.docker.container.image.v1", "vnd.oci.image.config.v1");
        replace_in_buffer(buffer, "vnd.docker.image.rootfs.diff.tar.gzip", "vnd.oci.image.layer.v1.tar+gzip");
        fputs(buffer, renamed);
    }
    fclose(old);
    fclose(renamed);

    // Remove the old one and rename the new one
    remove(fname_manifest.buffer);
    if(rlyeh_move_file(fname_tmp.buffer, fname_manifest.buffer) != 0) {
        SAH_TRACEZ_ERROR(ME, "Couldnt move file: %s -> %s", fname_tmp.buffer, fname_manifest.buffer);
        goto exit;
    }
    ret = 0;
exit:
    // Clean strings
    amxc_string_clean(&fname_index);
    amxc_string_clean(&fname_manifest);
    amxc_string_clean(&fname_tmp);
    return ret;
}

void rlyeh_copy_data_init(rlyeh_copy_data_t* data) {
    if(!data) {
        return;
    }
    memset((void*) data, 0, sizeof(rlyeh_copy_data_t));
}

void rlyeh_copy_data_clean(rlyeh_copy_data_t* data) {
    if(!data) {
        return;
    }
    free(data->duid);
    free(data->source);
    free(data->version);
    free(data->destination);
    free(data->dest_shared_blob_dir);
    free(data->username);
    free(data->password);
    free(data->signature_url_overload);
    free(data->ro_shared_blob_dir);
    free(data->images_location);
    rlyeh_copy_data_init(data);
}

void rlyeh_copy_output_directories_init(rlyeh_copy_output_directories_t* data) {
    amxc_string_init(&data->image, 0);
    amxc_string_init(&data->blobs, 0);
    amxc_string_init(&data->ro_blobs, 0);
}

void rlyeh_copy_output_directories_clean(rlyeh_copy_output_directories_t* data) {
    amxc_string_clean(&data->image);
    amxc_string_clean(&data->blobs);
    amxc_string_clean(&data->ro_blobs);
}

rlyeh_status_t rlyeh_copy(rlyeh_copy_data_t* data, char* err_msg) {
    rlyeh_status_t retval = RLYEH_NO_ERROR;
    rlyeh_copy_output_directories_t output_dir;
    rlyeh_image_parameters_t src_parameters;
    rlyeh_image_parameters_t dest_parameters;
    rlyeh_imagespec_manifest_element_t manifest;

    // Initialize variables
    rlyeh_copy_output_directories_init(&output_dir);
    rlyeh_image_parameters_init(&src_parameters);
    rlyeh_image_parameters_init(&dest_parameters);
    rlyeh_imagespec_manifest_element_init(&manifest);

    retval = print_parse_input(data, &src_parameters, &dest_parameters, err_msg);
    ASSERT_SUCCESS_NL(retval, goto exit);

    retval = create_output_directories(data, &dest_parameters, &output_dir, err_msg);
    ASSERT_SUCCESS_NL(retval, goto exit);

    retval = rlyeh_get_authentication_data(&src_parameters, err_msg);
    ASSERT_SUCCESS_NL(retval, goto exit);

    retval = download_image_files(&src_parameters, &manifest, &output_dir, err_msg);
    ASSERT_SUCCESS_NL(retval, goto exit);

    // Index.json generation
    if(STRING_EMPTY(manifest.version)) {
        // Fill with the version we got from the dest uri
        manifest.version = strdup(dest_parameters.version);
    }

    // set version in copy data, so caller knows the version at least
    data->version = strdup(manifest.version);

    if(STRING_EMPTY(manifest.name)) {
        // Ref name probably not set so using the one from the src uri
        manifest.name = strdup(src_parameters.image_name);
    }

    manifest.uri = strdup(data->source);
    manifest.duid = strdup(data->duid);
    manifest.mark_for_removal = false;

    // return proper image disk location
    if(rlyeh_image_index_file_build_from_manifest(output_dir.image.buffer, &manifest) < 1) {
        set_err_msg(err_msg, "Failed to build image index file [%s:%s] %s", manifest.duid, manifest.version, output_dir.image.buffer);
        retval = RLYEH_ERROR_CANNOT_CREATE_INDEX_MANIFEST;
        goto exit;
    }

    if(rlyeh_image_oci_layout_create(output_dir.image.buffer)) {
        set_err_msg(err_msg, "Failed to create image oci layout: %s", output_dir.image.buffer);
        retval = RLYEH_ERROR_CANNOT_CREATE_OCI_LAYOUT_FILE;
        goto exit;
    }

    //Patch mediatype for cthulhu
    if(patch_mediatype(&output_dir, manifest.version, manifest.duid)) {
        set_err_msg(err_msg, "Failed to patch image: %s", output_dir.image.buffer);
        retval = RLYEH_ERROR_CANNOT_PATCH_MEDIATYPE;
        goto exit;
    }

exit:
    rlyeh_copy_output_directories_clean(&output_dir);
    rlyeh_image_parameters_clean(&src_parameters);
    rlyeh_image_parameters_clean(&dest_parameters);
    rlyeh_imagespec_manifest_element_clean(&manifest);
    return retval;
}

int rlyeh_get_creds_from_cmd(rlyeh_copy_data_t* data, const char* userstring) {
    int ret = -2;
    char* cpyuserstring;
    char* username = NULL;
    char* password = NULL;

    when_null(userstring, exit);
    when_null(data, exit);

    cpyuserstring = strdup(userstring);

    username = strtok_r(cpyuserstring, ":", &password);
    if(username) {
        data->username = strdup(username);
        ret++;
        if(!STRING_EMPTY(password)) {
            data->password = strdup(password);
            ret++;
        }
    }

    free(cpyuserstring);
exit:
    return ret;
}
