all: $(TARGET)

run: $(TARGET)
	set -o pipefail; valgrind --leak-check=full --show-leak-kinds=all --error-exitcode=1 --suppressions=../valgrind.supp  ./$< 2>&1 | tee -a $(OBJDIR)/unit_test_results.txt;

$(TARGET): $(OBJECTS)
	$(CC) -o $@ $(OBJECTS) $(LDFLAGS)

-include $(OBJECTS:.o=.d)

$(OBJDIR)/%.o: ./%.c | $(OBJDIR)/
	$(CC) $(CFLAGS) -c -o $@ $<
	@$(CC) $(CFLAGS) -MM -MP -MT '$(@) $(@:.o=.d)' -MF $(@:.o=.d) $(<)

$(OBJDIR)/%.o: $(SRCDIR)/%.c | $(OBJDIR)/
	$(CC) $(CFLAGS) -c -o $@ $<
	@$(CC) $(CFLAGS) -MM -MP -MT '$(@) $(@:.o=.d)' -MF $(@:.o=.d) $(<)

$(OBJDIR)/:
	mkdir -p $@

clean:
ifneq ($(TARGET),)
	rm -rf $(TARGET)
else
	$(warning TARGET is empty)
endif
ifneq ($(OBJDIR),)
	rm -rf $(OBJDIR)/*
else
	$(warning OBJDIR is empty)
endif

.PHONY: clean $(OBJDIR)/
