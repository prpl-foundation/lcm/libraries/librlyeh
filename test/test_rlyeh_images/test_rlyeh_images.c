/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <sys/stat.h>
#include <dirent.h>
#include <fcntl.h>

#include <amxc/amxc.h>
#include <rlyeh/rlyeh.h>
#include <rlyeh_defines_priv.h>

#include <debug/sahtrace.h>

#include "test_rlyeh_images.h"

static void check_index(const char* filename, rlyeh_imagespec_manifest_element_t* data) {
    parser_error err;
    size_t it_manifests = 0;
    assert_non_null(filename);
    assert_non_null(data);
    image_spec_schema_image_index_schema* index_schema = image_spec_schema_image_index_schema_parse_file(filename, 0, &err);
    assert_non_null(index_schema);
    int ret = rlyeh_get_it_from_index(&it_manifests, index_schema, data->version, data->duid);
    assert_int_equal(ret, 0);
    assert_non_null(data->digest);
    assert_non_null(index_schema->manifests);
    assert_non_null(index_schema->manifests[it_manifests]);
    assert_non_null(index_schema->manifests[it_manifests]->digest);
    if(strncmp(data->digest, "sha256:", 7) == 0) {
        assert_string_equal(data->digest, index_schema->manifests[it_manifests]->digest);
    } else {
        assert_string_equal(data->digest, index_schema->manifests[it_manifests]->digest + 7); // <- to remove the "sha256:" prefix
    }
    assert_int_equal(data->manifest_size, index_schema->manifests[it_manifests]->size);
    assert_non_null(index_schema->manifests[it_manifests]->annotations);
    for(size_t it_annotations = 0; it_annotations < index_schema->manifests[it_manifests]->annotations->len; it_annotations++) {
        const char* key = index_schema->manifests[it_manifests]->annotations->keys[it_annotations];
        const char* value = index_schema->manifests[it_manifests]->annotations->values[it_annotations];
        if(strcmp(RLYEH_ANNOTATION_LCM_URI, key) == 0) {
            assert_string_equal(data->uri, value);
        } else if(strcmp(RLYEH_ANNOTATION_LCM_DUID, key) == 0) {
            assert_string_equal(data->duid, value);
        } else if(strcmp(RLYEH_ANNOTATION_LCM_MAKRFORREMOVAL, key) == 0) {
            if((value[0] == '1') && (value[1] == '\0')) {
                assert_true(data->mark_for_removal);
            } else if((value[0] == '0') && (value[1] == '\0')) {
                assert_false(data->mark_for_removal);
            } else {
                assert_false(true);
            }
        } else if(strcmp(RLYEH_ANNOTATION_OCI_IMAGE_TITLE, key) == 0) {
            assert_string_equal(data->name, value);
        } else if(strcmp(RLYEH_ANNOTATION_LCM_DUID, key) == 0) {
            assert_string_equal(data->duid, value);
        } else if(strcmp(RLYEH_ANNOTATION_OCI_IMAGE_DESCRIPTION, key) == 0) {
            assert_string_equal(data->description, value);
        }
    }

    free_image_spec_schema_image_index_schema(index_schema);
}

void test_rlyeh_image_index_file_build_from_manifest(UNUSED void** state) {
    const char* image_location = "/tmp/";
    rlyeh_imagespec_manifest_element_t manifest_ele;

    // Init sahtrace
    sahTraceSetLevel(500);
    sahTraceAddZone(500, "rlyeh_utils");
    sahTraceAddZone(500, "rlyeh_images");
    sahTraceAddZone(500, __func__);
    sahTraceOpen(__FILE__, TRACE_TYPE_STDOUT);
    SAH_TRACEZ_INFO(__func__, "sahTrace initialized");

    rlyeh_imagespec_manifest_element_init(&manifest_ele);
    manifest_ele.manifest_size = 100;
    manifest_ele.digest = strdup("47046d47361fd624bd80d54e61d4ec7b1a92a06afe2d4b8bded9319d428558b4");
    manifest_ele.version = strdup("version1");
    manifest_ele.name = strdup("imagenamerandom");
    manifest_ele.uri = strdup("docker://random.com/imagename:version1");
    manifest_ele.duid = strdup("duid1");
    manifest_ele.description = strdup("");
    manifest_ele.vendor = strdup("");
    manifest_ele.mark_for_removal = false;

    remove("/tmp/index.json");

    SAH_TRACEZ_INFO(__func__, "Build index 1");
    rlyeh_image_index_file_build_from_manifest(image_location, &manifest_ele);
    print_file(__func__, "/tmp/index.json");
    check_index("/tmp/index.json", &manifest_ele);

    SAH_TRACEZ_INFO(__func__, "Build index 1 - again");
    // update the mark for removal
    manifest_ele.mark_for_removal = true;
    rlyeh_image_index_file_build_from_manifest(image_location, &manifest_ele);
    print_file(__func__, "/tmp/index.json");
    check_index("/tmp/index.json", &manifest_ele);

    SAH_TRACEZ_INFO(__func__, "Build index 2");
    // update the mark for removal
    free(manifest_ele.duid);
    manifest_ele.duid = strdup("duid2");
    manifest_ele.mark_for_removal = true;
    rlyeh_image_index_file_build_from_manifest(image_location, &manifest_ele);
    print_file(__func__, "/tmp/index.json");
    check_index("/tmp/index.json", &manifest_ele);

    SAH_TRACEZ_INFO(__func__, "Build index 2 - again");
    // update the mark for removal
    manifest_ele.mark_for_removal = true;
    rlyeh_image_index_file_build_from_manifest(image_location, &manifest_ele);
    print_file(__func__, "/tmp/index.json");
    check_index("/tmp/index.json", &manifest_ele);

    SAH_TRACEZ_INFO(__func__, "Build index 1 - again - again");
    // update the mark for removal
    free(manifest_ele.duid);
    manifest_ele.duid = strdup("duid1");
    manifest_ele.mark_for_removal = false;
    rlyeh_image_index_file_build_from_manifest(image_location, &manifest_ele);
    print_file(__func__, "/tmp/index.json");
    check_index("/tmp/index.json", &manifest_ele);

    SAH_TRACEZ_INFO(__func__, "Build index 2 - again - again");
    // update the mark for removal
    free(manifest_ele.duid);
    manifest_ele.duid = strdup("duid2");
    manifest_ele.mark_for_removal = false;
    rlyeh_image_index_file_build_from_manifest(image_location, &manifest_ele);
    print_file(__func__, "/tmp/index.json");
    check_index("/tmp/index.json", &manifest_ele);

    SAH_TRACEZ_INFO(__func__, "Build index 3");
    // update the mark for removal
    free(manifest_ele.duid);
    manifest_ele.duid = strdup("duid3");
    manifest_ele.mark_for_removal = true;
    rlyeh_image_index_file_build_from_manifest(image_location, &manifest_ele);
    check_index("/tmp/index.json", &manifest_ele);
    print_file(__func__, "/tmp/index.json");
    remove("/tmp/index.json");

    SAH_TRACEZ_INFO(__func__, "Build index 4");
    // Add a second version with a differnt duid and check for it
    free(manifest_ele.digest);
    manifest_ele.digest = strdup("sha256:47046d47361fd624bd80d54e61d4ec7b1a92a06afe2d4b8bded9319d428558b4");
    free(manifest_ele.duid);
    manifest_ele.duid = strdup("duid3");
    free(manifest_ele.version);
    manifest_ele.version = strdup("version2");
    manifest_ele.mark_for_removal = true;
    free(manifest_ele.vendor);
    manifest_ele.vendor = strdup("prpl");
    free(manifest_ele.uri);
    manifest_ele.uri = strdup("docker://prplimages.com/prplimage:version2");
    free(manifest_ele.name);
    manifest_ele.name = strdup("prplimage");
    rlyeh_image_index_file_build_from_manifest(image_location, &manifest_ele);
    print_file(__func__, "/tmp/index.json");
    check_index("/tmp/index.json", &manifest_ele);

    SAH_TRACEZ_INFO(__func__, "Build index 5");
    // Add a third version of the same duid and check for it
    free(manifest_ele.duid);
    manifest_ele.duid = strdup("duid3");
    free(manifest_ele.version);
    manifest_ele.version = strdup("version3");
    manifest_ele.mark_for_removal = false;
    free(manifest_ele.uri);
    manifest_ele.uri = strdup("docker://prplimages.com/prplimage:version3");
    rlyeh_image_index_file_build_from_manifest(image_location, &manifest_ele);
    print_file(__func__, "/tmp/index.json");
    check_index("/tmp/index.json", &manifest_ele);

    SAH_TRACEZ_INFO(__func__, "Build index 6");
    // Add a third version of the same duid and check for it
    free(manifest_ele.duid);
    manifest_ele.duid = strdup("duid4");
    free(manifest_ele.version);
    manifest_ele.version = strdup("version1");
    manifest_ele.mark_for_removal = false;
    free(manifest_ele.uri);
    manifest_ele.uri = strdup("docker://prpl2images.com/prplimage:version2");
    free(manifest_ele.vendor);
    manifest_ele.vendor = strdup("prpl2");
    rlyeh_image_index_file_build_from_manifest(image_location, &manifest_ele);
    print_file(__func__, "/tmp/index.json");
    check_index("/tmp/index.json", &manifest_ele);

    SAH_TRACEZ_INFO(__func__, "Build index 7");
    // Add a third version of the same duid and check for it
    free(manifest_ele.duid);
    manifest_ele.duid = strdup("duid5");
    free(manifest_ele.version);
    manifest_ele.version = strdup("version3");
    free(manifest_ele.uri);
    manifest_ele.mark_for_removal = true;
    manifest_ele.uri = strdup("docker://prpl2images.com/prplimage:version3");
    rlyeh_image_index_file_build_from_manifest(image_location, &manifest_ele);
    print_file(__func__, "/tmp/index.json");
    check_index("/tmp/index.json", &manifest_ele);

    SAH_TRACEZ_INFO(__func__, "Build index 8");
    // Add a third version of the same duid and check for it
    free(manifest_ele.version);
    manifest_ele.version = strdup("version5");
    free(manifest_ele.uri);
    manifest_ele.uri = strdup("docker://prpl2images.com/prplimage:version5");
    manifest_ele.mark_for_removal = false;
    rlyeh_image_index_file_build_from_manifest(image_location, &manifest_ele);
    print_file(__func__, "/tmp/index.json");
    check_index("/tmp/index.json", &manifest_ele);

    SAH_TRACEZ_INFO(__func__, "Build index 9");
    // Add a third version of the same duid and check for it
    free(manifest_ele.duid);
    manifest_ele.duid = strdup("duid6");
    rlyeh_image_index_file_build_from_manifest(image_location, &manifest_ele);
    print_file(__func__, "/tmp/index.json");
    check_index("/tmp/index.json", &manifest_ele);

    SAH_TRACEZ_INFO(__func__, "Build index 10");
    // Add a third version of the same duid and check for it
    free(manifest_ele.duid);
    manifest_ele.duid = strdup("duid7");
    rlyeh_image_index_file_build_from_manifest(image_location, &manifest_ele);
    print_file(__func__, "/tmp/index.json");
    check_index("/tmp/index.json", &manifest_ele);

    SAH_TRACEZ_INFO(__func__, "Build index 10 - again");
    manifest_ele.mark_for_removal = true;
    rlyeh_image_index_file_build_from_manifest(image_location, &manifest_ele);
    print_file(__func__, "/tmp/index.json");
    check_index("/tmp/index.json", &manifest_ele);

    rlyeh_imagespec_manifest_element_clean(&manifest_ele);
    remove("/tmp/index.json");

    sahTraceClose();
}

void test_rlyeh_update_index_annotations(UNUSED void** state) {
    const char* image_location = "/tmp/";
    rlyeh_imagespec_manifest_element_t manifest_ele;

    // Init sahtrace
    sahTraceSetLevel(500);
    sahTraceAddZone(500, "rlyeh_utils");
    sahTraceAddZone(500, "rlyeh_images");
    sahTraceAddZone(500, __func__);
    sahTraceOpen(__FILE__, TRACE_TYPE_STDOUT);
    SAH_TRACEZ_INFO(__func__, "sahTrace initialized");

    rlyeh_imagespec_manifest_element_init(&manifest_ele);
    manifest_ele.manifest_size = 100;
    manifest_ele.digest = strdup("47046d47361fd624bd80d54e61d4ec7b1a92a06afe2d4b8bded9319d428558b4");
    manifest_ele.version = strdup("version1");
    manifest_ele.name = strdup("imagename");
    manifest_ele.uri = strdup("uri1");
    manifest_ele.duid = strdup("duid1");
    manifest_ele.description = strdup("");
    manifest_ele.mark_for_removal = false;
    manifest_ele.vendor = strdup("");

    remove("/tmp/index.json");

    SAH_TRACEZ_INFO(__func__, "Build index");
    rlyeh_image_index_file_build_from_manifest(image_location, &manifest_ele);
    print_file(__func__, "/tmp/index.json");
    check_index("/tmp/index.json", &manifest_ele);

    SAH_TRACEZ_INFO(__func__, "Update annotation " RLYEH_ANNOTATION_LCM_MAKRFORREMOVAL);
    rlyeh_update_index_annotations(image_location, manifest_ele.duid, manifest_ele.version, RLYEH_ANNOTATION_LCM_MAKRFORREMOVAL, "1");
    manifest_ele.mark_for_removal = true;
    print_file(__func__, "/tmp/index.json");
    check_index("/tmp/index.json", &manifest_ele);

    SAH_TRACEZ_INFO(__func__, "Update annotation " RLYEH_ANNOTATION_LCM_DUID);
    free(manifest_ele.duid);
    manifest_ele.duid = strdup("duid2");
    rlyeh_update_index_annotations(image_location, "duid1", manifest_ele.version, RLYEH_ANNOTATION_LCM_DUID, manifest_ele.duid);
    print_file(__func__, "/tmp/index.json");
    check_index("/tmp/index.json", &manifest_ele);

    SAH_TRACEZ_INFO(__func__, "Update annotation " RLYEH_ANNOTATION_LCM_URI);
    free(manifest_ele.uri);
    manifest_ele.uri = strdup("uri2");
    rlyeh_update_index_annotations(image_location, manifest_ele.duid, manifest_ele.version, RLYEH_ANNOTATION_LCM_URI, manifest_ele.uri);
    print_file(__func__, "/tmp/index.json");
    check_index("/tmp/index.json", &manifest_ele);

    SAH_TRACEZ_INFO(__func__, "Update annotation " RLYEH_ANNOTATION_OCI_IMAGE_DESCRIPTION);
    free(manifest_ele.description);
    manifest_ele.description = strdup("Fun description of the container");
    rlyeh_update_index_annotations(image_location, manifest_ele.duid, manifest_ele.version, RLYEH_ANNOTATION_OCI_IMAGE_DESCRIPTION, manifest_ele.description);
    print_file(__func__, "/tmp/index.json");
    check_index("/tmp/index.json", &manifest_ele);

    SAH_TRACEZ_INFO(__func__, "Update annotation " RLYEH_ANNOTATION_OCI_IMAGE_DESCRIPTION);
    free(manifest_ele.description);
    manifest_ele.description = strdup("Fun description of the container");
    rlyeh_update_index_annotations(image_location, manifest_ele.duid, manifest_ele.version, "NEW_KEY", "NEW_VALUE");
    print_file(__func__, "/tmp/index.json");
    check_index("/tmp/index.json", &manifest_ele);

    rlyeh_imagespec_manifest_element_clean(&manifest_ele);
    remove("/tmp/index.json");

    sahTraceClose();
}

void test_rlyeh_imagespec_manifest_element_get_from_annotations(UNUSED void** state) {
    rlyeh_imagespec_manifest_element_t manifest;
    const char* storage_location = "blobs/";

    // Init sahtrace
    sahTraceSetLevel(500);
    sahTraceAddZone(500, "rlyeh_utils");
    sahTraceAddZone(500, "rlyeh_images");
    sahTraceAddZone(500, __func__);
    sahTraceOpen(__FILE__, TRACE_TYPE_STDOUT);
    SAH_TRACEZ_INFO(__func__, "sahTrace initialized");

    SAH_TRACEZ_INFO(__func__, "Get annotations #1");
    rlyeh_imagespec_manifest_element_init(&manifest);
    assert_false(rlyeh_imagespec_manifest_element_get_from_annotations("images/image", storage_location, "duid1", "v1", &manifest));
    assert_string_equal("v1", manifest.version);
    assert_string_equal("image", manifest.name);
    assert_string_equal("prpl", manifest.vendor);
    assert_string_equal("image test image", manifest.description);
    assert_string_equal("duid1", manifest.duid);
    rlyeh_imagespec_manifest_element_clean(&manifest);

    SAH_TRACEZ_INFO(__func__, "Get annotations #2");
    rlyeh_imagespec_manifest_element_init(&manifest);
    assert_false(rlyeh_imagespec_manifest_element_get_from_annotations("images/image1", storage_location, "duid2", "latest", &manifest));
    assert_string_equal("duid2", manifest.duid);
    assert_string_equal("latest", manifest.version);
    assert_string_equal("image1", manifest.name);
    assert_string_equal("prpl", manifest.vendor);
    assert_string_equal("image1 test image", manifest.description);
    rlyeh_imagespec_manifest_element_clean(&manifest);

    SAH_TRACEZ_INFO(__func__, "Get annotations #3");
    rlyeh_imagespec_manifest_element_init(&manifest);
    assert_false(rlyeh_imagespec_manifest_element_get_from_annotations("images/library/alpine", storage_location, "duid7", "latest", &manifest));
    assert_string_equal("latest", manifest.version);
    assert_string_equal("duid7", manifest.duid);
    assert_null(manifest.name);
    assert_null(manifest.vendor);
    assert_null(manifest.description);
    rlyeh_imagespec_manifest_element_clean(&manifest);

    SAH_TRACEZ_INFO(__func__, "Get annotations #4");
    rlyeh_imagespec_manifest_element_init(&manifest);
    assert_false(rlyeh_imagespec_manifest_element_get_from_annotations("images/subdir1/subdir2/imagelonglonglonglonglonglonglonglonglonglonglonglong", storage_location, "duid6", "v2.1.0", &manifest));
    assert_string_equal("v2.1.0", manifest.version);
    assert_string_equal("subdir1/subdir2/image/imagelonglonglonglonglonglonglonglonglonglonglonglong", manifest.name);
    assert_string_equal("duid6", manifest.duid);
    assert_string_equal("prpl2", manifest.vendor);
    assert_string_equal("subdir1/subdir2/image/imagelonglonglonglonglonglonglonglonglonglonglonglong test image", manifest.description);
    rlyeh_imagespec_manifest_element_clean(&manifest);

    SAH_TRACEZ_INFO(__func__, "Get annotations #5");
    rlyeh_imagespec_manifest_element_init(&manifest);
    assert_true(rlyeh_imagespec_manifest_element_get_from_annotations("images/subdir1/subdir2/imagelonglonglonglonglonglonglonglonglonglonglonglong", storage_location, "duid99", "v2.1.0", &manifest));
    assert_null(manifest.version);
    assert_null(manifest.duid);
    assert_null(manifest.vendor);
    assert_null(manifest.description);
    rlyeh_imagespec_manifest_element_clean(&manifest);

    SAH_TRACEZ_INFO(__func__, "Get annotations #6");
    rlyeh_imagespec_manifest_element_init(&manifest);
    assert_true(rlyeh_imagespec_manifest_element_get_from_annotations("images/subdir1/subdir2/imagelonglonglonglonglonglonglonglonglonglonglonglong", storage_location, "duid6", "v999999xx", &manifest));
    assert_null(manifest.version);
    assert_null(manifest.duid);
    assert_null(manifest.vendor);
    assert_null(manifest.description);
    rlyeh_imagespec_manifest_element_clean(&manifest);

    SAH_TRACEZ_INFO(__func__, "Get annotations #7");
    rlyeh_imagespec_manifest_element_init(&manifest);
    assert_true(rlyeh_imagespec_manifest_element_get_from_annotations("/tmp/", storage_location, "duid6", "v999999xx", &manifest));
    assert_null(manifest.version);
    assert_null(manifest.duid);
    assert_null(manifest.vendor);
    assert_null(manifest.description);
    rlyeh_imagespec_manifest_element_clean(&manifest);

    SAH_TRACEZ_INFO(__func__, "Get annotations #7");
    rlyeh_imagespec_manifest_element_init(&manifest);
    assert_true(rlyeh_imagespec_manifest_element_get_from_annotations("/", storage_location, "duid6", "v999999xx", &manifest));
    assert_null(manifest.version);
    assert_null(manifest.duid);
    assert_null(manifest.vendor);
    assert_null(manifest.description);
    rlyeh_imagespec_manifest_element_clean(&manifest);

    sahTraceClose();
}

typedef struct _image_description {
    const char* disklocation;
    const char* name;
    const char* version;
    const char* duid;
    const char* digest;
    const char* uri;
    const char* vendor;
    const char* description;
    unsigned int manifest_size;
    bool mark_for_removal;
} image_description_t;

typedef struct _test_rlyeh_images_list_build_args {
    image_description_t* list_of_images;
    size_t amount_of_images;
    bool* data;
} test_rlyeh_images_list_build_args_t;

static void test_rlyeh_images_list_build_inner(rlyeh_imagespec_index_t* index, rlyeh_imagespec_manifest_element_t* manifest, void* args) {
    test_rlyeh_images_list_build_args_t* parsed_args = (test_rlyeh_images_list_build_args_t*) args;
    for(size_t it = 0; it < parsed_args->amount_of_images; it++) {
        if(parsed_args->data[it]) {
            continue;
        }

        if(parsed_args->list_of_images[it].manifest_size != manifest->manifest_size) {
            continue;
        }

        if(strcmp(parsed_args->list_of_images[it].duid, manifest->duid) != 0) {
            continue;
        }

        if(strcmp(parsed_args->list_of_images[it].version, manifest->version) != 0) {
            continue;
        }

        if(strcmp(parsed_args->list_of_images[it].disklocation, index->image_disk_location) != 0) {
            continue;
        }

        if(strcmp(parsed_args->list_of_images[it].name, manifest->name) != 0) {
            continue;
        }

        if(strcmp(parsed_args->list_of_images[it].version, manifest->version) != 0) {
            continue;
        }

        if(strcmp(parsed_args->list_of_images[it].digest, manifest->digest) != 0) {
            continue;
        }

        if(strcmp(parsed_args->list_of_images[it].uri, manifest->uri) != 0) {
            continue;
        }

        if(manifest->vendor) {
            if(strcmp(parsed_args->list_of_images[it].vendor, manifest->vendor) != 0) {
                continue;
            }
        }

        if(manifest->description) {
            if(strcmp(parsed_args->list_of_images[it].description, manifest->description) != 0) {
                continue;
            }
        }

        parsed_args->data[it] = true;
    }
}

void test_rlyeh_images_list_build(UNUSED void** state) {
    int ret = 0;
    bool* data = NULL;
    rlyeh_images_list_t images_list;
    test_rlyeh_images_list_build_args_t args;
    image_description_t list_of_images[] = {
        {
            .disklocation = "images/image",
            .name = "image",
            .version = "v1",
            .uri = "docker://prplimages.com/image:v1",
            .duid = "duid1",
            .digest = "sha256:81b72167a6f8a97f99a1066b7c4647baa4f7d645f35fac7be36d937598827807",
            .manifest_size = 721,
            .vendor = "prpl",
            .description = "image test image",
            .mark_for_removal = false
        },
        {
            .disklocation = "images/image1",
            .name = "image1",
            .version = "latest",
            .uri = "docker://prplimages.com/image1",
            .duid = "duid2",
            .digest = "sha256:a8f9bf90257836c372a4aa4b14ede4881722aaca07396c45a43f3c95aaf548d6",
            .manifest_size = 723,
            .vendor = "prpl",
            .description = "image1 test image",
            .mark_for_removal = false
        },
        {
            .disklocation = "images/image_manifestname",
            .name = "image_manifestname",
            .version = "v1.2.7",
            .uri = "docker://prplserver.com/image_manifestname:v1.2.7",
            .duid = "duid3", .digest = "sha256:bfb3d762534d3f4dea65d5d2ba03ba84acb74158116482d45f02c7b723056464",
            .manifest_size = 747,
            .vendor = "prpl",
            .description = "image_manifestname test image",
            .mark_for_removal = false
        },
        {
            .disklocation = "images/subdir1/subdir2/image",
            .name = "subdir1/subdir2/image",
            .version = "v99.99.99",
            .uri = "docker://prplimages.com/subdir1/subdir2/image:v99.99.99",
            .duid = "duid4",
            .digest = "sha256:4c3d293635d566a88e5573d86bccd3e78c476b06eab84ef7dfaf8589faeb45f9",
            .manifest_size = 537,
            .description = "subdir1/subdir2/image test image",
            .mark_for_removal = false,
            .vendor = "prpl"
        },
        {
            .disklocation = "images/subdir1/subdir2/imagelonglonglonglonglonglonglonglonglonglonglonglong",
            .name = "subdir1/subdir2/image/imagelonglonglonglonglonglonglonglonglonglonglonglong",
            .version = "v1.99.7",
            .uri = "docker://prpl2images.com/subdir1/subdir2/image/imagelonglonglonglonglonglonglonglonglonglonglonglong:v1.99.7",
            .duid = "duid5",
            .digest = "sha256:016f464eb1a990a92f5319bbb01b53bd96d5ea618520760325b746870463b3c3",
            .manifest_size = 986,
            .description = "subdir1/subdir2/image/imagelonglonglonglonglonglonglonglonglonglonglonglong test image",
            .mark_for_removal = false,
            .vendor = "prpl2"
        },
        {
            .disklocation = "images/subdir1/subdir2/imagelonglonglonglonglonglonglonglonglonglonglonglong",
            .name = "subdir1/subdir2/image/imagelonglonglonglonglonglonglonglonglonglonglonglong",
            .version = "v2.1.0",
            .uri = "docker://prpl2images.com/subdir1/subdir2/image/imagelonglonglonglonglonglonglonglonglonglonglonglong:v2.1.0",
            .duid = "duid6",
            .digest = "sha256:9f2814c621ca38c1fec307a340a4c94af2a2304b6aedd3e295d46a4dbef0002d",
            .manifest_size = 862,
            .description = "subdir1/subdir2/image/imagelonglonglonglonglonglonglonglonglonglonglonglong test image",
            .mark_for_removal = true,
            .vendor = "prpl2"
        },
        {
            .disklocation = "images/subdir1/subdir2/imagelonglonglonglonglonglonglonglonglonglonglonglong",
            .name = "subdir1/subdir2/image/imagelonglonglonglonglonglonglonglonglonglonglonglong",
            .version = "v2.1.1",
            .uri = "docker://prpl2images.com/subdir1/subdir2/image/imagelonglonglonglonglonglonglonglonglonglonglonglong:v2.1.1",
            .duid = "duid6",
            .digest = "sha256:9f2814c621ca38c1fec307a340a4c94af2a2304b6aedd3e295d46a4dbef0002d",
            .manifest_size = 862,
            .description = "subdir1/subdir2/image/imagelonglonglonglonglonglonglonglonglonglonglonglong test image",
            .mark_for_removal = true,
            .vendor = "prpl2"
        },
        {
            .disklocation = "images/library/alpine",
            .name = "library/alpine",
            .version = "latest",
            .uri = "docker://docker.io/library/alpine",
            .duid = "duid7",
            .digest = "sha256:2af82520fe79b5d2fd39b8122a2d647ff8fa62b8704310b3424f3ba259f85598",
            .manifest_size = 912,
            .mark_for_removal = true,
        },
    };
    size_t amount_of_images = sizeof(list_of_images) / sizeof(image_description_t);
    data = (bool*) calloc(amount_of_images, sizeof(bool));
    args.list_of_images = list_of_images;
    args.amount_of_images = amount_of_images;
    args.data = data;

    // Init sahtrace
    sahTraceSetLevel(500);
    sahTraceAddZone(500, "rlyeh_utils");
    sahTraceAddZone(500, "rlyeh_images");
    sahTraceAddZone(500, __func__);
    sahTraceOpen(__FILE__, TRACE_TYPE_STDOUT);
    SAH_TRACEZ_INFO(__func__, "sahTrace initialized");

    rlyeh_images_list_init(&images_list);
    ret = rlyeh_images_list_build(&images_list, "/tmp/test_rlyeh_images_list_build/fictional");
    assert_int_equal(ret, 0);
    rlyeh_images_list_clean(&images_list);

    rlyeh_images_list_init(&images_list);
    ret = rlyeh_images_list_build(&images_list, NULL);
    assert_int_equal(ret, -1);
    rlyeh_images_list_clean(&images_list);

    rlyeh_images_list_init(&images_list);
    ret = rlyeh_images_list_build(NULL, "images");
    assert_int_equal(ret, -1);
    rlyeh_images_list_clean(&images_list);

    rlyeh_images_list_init(&images_list);
    ret = rlyeh_images_list_build(&images_list, "images");
    assert_int_equal(ret, amount_of_images);

    rlyeh_loop_over_images_list(&images_list, test_rlyeh_images_list_build_inner, &args);

    for(size_t it = 0; it < amount_of_images; it++) {
        SAH_TRACEZ_INFO(__func__, "Checking for image %u [%s:%s] %s", it, list_of_images[it].duid, list_of_images[it].version, list_of_images[it].name);
        assert_true(data[it]);
    }

    rlyeh_images_list_clean(&images_list);
    free(data);
    sahTraceClose();
}
