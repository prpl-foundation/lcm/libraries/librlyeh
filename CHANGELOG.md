# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v1.3.0 - 2024-10-18(14:59:51 +0000)

### Other

- [Rlyeh] Refactor to have singular code paths for parsing the library of images
- LCM librlyeh unit tests not executed

## Release v1.2.0 - 2024-07-15(13:01:28 +0000)

### Other

- LCM container signature verification

## Release v1.1.5 - 2024-05-06(13:29:58 +0000)

### Other

- do not print password in the logs

## Release v1.1.4 - 2024-02-01(12:56:38 +0000)

### Other

- [librlyeh] Add support for opensslv3

## Release v1.1.3 - 2023-11-13(15:50:01 +0000)

### Other

- Support bearer auth with username/password

## Release v1.1.2 - 2023-10-04(10:21:19 +0000)

### Other

- Opensource component
- - Update license

## Release v1.1.1 - 2023-09-07(10:12:59 +0000)

### Other

- Make component available on gitlab.softathome.com

## Release v1.1.0 - 2023-07-12(11:58:42 +0000)

### Other

- LCM containers onboarding solution - runtime

## Release v1.0.1 - 2023-06-23(11:21:09 +0000)

### Other

- Move signature from Annotations to installDU() parameter

## Release v1.0.0 - 2023-06-02(11:01:20 +0000)

### Other

- DU/EU Unique Identifier

## Release v0.4.31 - 2023-03-10(08:10:07 +0000)

### Other

- safeguard against empty manifest filenames

## Release v0.4.30 - 2023-02-27(10:15:21 +0000)

### Other

- [librlyeh] Use STAGING_LIBDIR instead of LIBDIR in src/makefile

## Release v0.4.29 - 2023-02-24(14:29:50 +0000)

### Other

- Rlyeh crash when triggering the garbage collector
- [Rlyeh] Add feature to disable certificate verification

## Release v0.4.28 - 2023-02-02(09:16:35 +0000)

### Other

- [Rlyeh] curl write callback does not handle error cases

## Release v0.4.27 - 2023-01-27(10:38:54 +0000)

## Release v0.4.26 - 2023-01-27(08:31:13 +0000)

### Other

- Rlyeh could crash when an invalid index file was read

## Release v0.4.25 - 2023-01-18(16:33:48 +0000)

### Other

- Add define for RemainingDiskSpaceBytes

## Release v0.4.24 - 2023-01-10(14:30:09 +0000)

## Release v0.4.23 - 2022-10-21(15:19:10 +0000)

### Other

- allow for several subdirs on repo

## Release v0.4.22 - 2022-10-21(09:19:25 +0000)

### Other

- installDU returns informative data when failed

## Release v0.4.21 - 2022-10-07(12:58:55 +0000)

### Other

- Remove warnings from tests

## Release v0.4.20 - 2022-10-05(20:12:33 +0000)

### Fixes

- 2 simultaneous InstallDu commands could cause a crash

## Release v0.4.19 - 2022-09-02(07:23:25 +0000)

### Other

- rlyeh should return an error the auth failed during Rlyeh.pull()

## Release v0.4.18 - 2022-06-30(16:31:35 +0000)

### Other

- Fix possible segfault

## Release v0.4.17 - 2022-06-07(09:06:34 +0000)

### Other

- Signature location shall follow image path & version

## Release v0.4.16 - 2022-04-06(13:21:46 +0000)

### Other

- : Change bundle name algorithm due to BundleName too long

## Release v0.4.15 - 2022-04-04(08:34:45 +0000)

## Release v0.4.14 - 2022-03-23(10:44:09 +0000)

### Other

- Error with notifications when installing 2 applications at the same time

## Release v0.4.13 - 2022-03-03(09:23:52 +0000)

### Other

- Incorrect Error handling in case of installing an already present DU

## Release v0.4.12 - 2022-03-01(14:56:25 +0000)

### Other

- Rlyeh crash when trying to uninstall one of the two installed DUs

## Release v0.4.11 - 2022-01-10(18:10:36 +0000)

## Release v0.4.10 - 2022-01-07(13:55:53 +0000)

## Release v0.4.9 - 2021-12-23(10:37:49 +0000)

### Fixes

- fix NULL pointer access

## Release v0.4.8 - 2021-12-15(14:30:49 +0000)

## Release v0.4.7 - 2021-11-30(11:24:31 +0000)

### Other

- [Rlyeh] Add support for docker.io

## Release v0.4.6 - 2021-11-24(10:14:19 +0000)

### Other

- [Rlyeh] Add support for docker.io

## Release v0.4.5 - 2021-11-05(13:06:03 +0000)

### Other

- libRlyeh : keep track of shared layers

## Release v0.4.4 - 2021-11-04(15:32:55 +0000)

### Other

- [Rlyeh] Implement 'Status' parameter

## Release v0.4.3 - 2021-10-27(12:58:03 +0000)

### Other

- [Rlyeh] Verify the OCI image signature

## Release v0.4.2 - 2021-09-27(09:03:17 +0000)

### Fixes

- Fix rename for old compilers

## Release v0.4.1 - 2021-09-24(09:32:54 +0000)

### Fixes

- [librlyeh] rename on cross device links not working

## Release v0.4.0 - 2021-09-23(11:58:30 +0000)

### New

- Issue [IOT-886] : [Rlyeh] Verify checksum after pulling image's blobs

## Release v0.3.3 - 2021-09-13(09:22:13 +0000)

### Fixes

- rework rlyeh_remove

## Release v0.3.2 - 2021-09-06(10:31:54 +0000)

### Fixes

- add a config for external

## Release v0.3.1 - 2021-09-06(10:04:00 +0000)

### Other

- Add changenlog
- Update .gitlab-ci.yml
- [lib rlyeh] Add remove to ckopeo

## Release v0.3.0 - 2021-09-03(09:48:30 +0000)

### New

- Initial release
