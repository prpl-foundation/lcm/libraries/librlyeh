/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#if !defined(__RLYEH_IMAGES_H__)
#define __RLYEH_IMAGES_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <libocispec/image_spec_schema_image_index_schema.h>
#include <libocispec/image_spec_schema_image_manifest_schema.h>
#include <amxc/amxc.h>

typedef struct _imagespec_manifest_element {
    amxc_llist_it_t it;
    char* version;
    char* duid;
    char* name;
    char* vendor;
    char* uri;
    char* description;
    char* digest;
    bool mark_for_removal;
    unsigned int manifest_size;
} rlyeh_imagespec_manifest_element_t;

typedef struct _imagespec_index {
    amxc_llist_it_t it;
    char* image_disk_location;
    amxc_llist_t manifest_elements;
} rlyeh_imagespec_index_t;

typedef amxc_llist_t rlyeh_images_list_t;

void rlyeh_images_list_init(rlyeh_images_list_t* images_list);
void rlyeh_images_list_clean(rlyeh_images_list_t* images_list);

void rlyeh_imagespec_manifest_element_init(rlyeh_imagespec_manifest_element_t* data);
void rlyeh_imagespec_index_init(rlyeh_imagespec_index_t* data);
void rlyeh_imagespec_manifest_element_clean(rlyeh_imagespec_manifest_element_t* item);
void rlyeh_imagespec_index_clean(rlyeh_imagespec_index_t* index);

int rlyeh_images_list_build(rlyeh_images_list_t* images_list, const char* image_path);

void rlyeh_remove_manifest_element_from_index(rlyeh_imagespec_manifest_element_t* manifest);

typedef void (rlyeh_image_list_callback_t)(rlyeh_imagespec_index_t*, rlyeh_imagespec_manifest_element_t*, void*);
void rlyeh_loop_over_images_list(rlyeh_images_list_t* images_list, rlyeh_image_list_callback_t fn, void* args);

int rlyeh_image_index_file_build_from_manifest(const char* image_location, const rlyeh_imagespec_manifest_element_t* manifest);
void fill_manifest_element_from_manifest(image_spec_schema_image_index_schema_manifests_element* manifest_to_fill, const rlyeh_imagespec_manifest_element_t* manifest);

void image_spec_schema_image_index_manifest_element_clean(image_spec_schema_image_index_schema_manifests_element* manifest);

int rlyeh_update_index_annotations(const char* disk_location,
                                   const char* duid,
                                   const char* version,
                                   const char* annotation_key,
                                   const char* annotation_value);

void fill_imagespec_manifest_element_data_from_manifest_schema(const image_spec_schema_image_manifest_schema* image_manifest, rlyeh_imagespec_manifest_element_t* item);

int rlyeh_imagespec_manifest_element_get_from_annotations(const char* disk_location,
                                                          const char* storage_location,
                                                          const char* duid,
                                                          const char* version,
                                                          rlyeh_imagespec_manifest_element_t* manifest);

int rlyeh_save_images_from_list(rlyeh_images_list_t* images_list);
int rlyeh_remove_index_files_from_list(rlyeh_images_list_t* images_list);

#ifdef __cplusplus
}
#endif

#endif // __RLYEH_IMAGES_H__
