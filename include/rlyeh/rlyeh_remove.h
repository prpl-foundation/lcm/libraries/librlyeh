/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#if !defined(__RLYEH_REMOVE_H__)
#define __RLYEH_REMOVE_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <amxc/amxc_llist.h>
#include <amxc/amxc_string.h>
#include <rlyeh/rlyeh_images.h>

typedef struct _rlyeh_remove_data {
    char* version;
    char* duid;
    char* imageLocation;
    char* imageDiskLocation;
} rlyeh_remove_data_t;

void rlyeh_remove_data_init(rlyeh_remove_data_t* data);
void rlyeh_remove_data_clean(rlyeh_remove_data_t* data);

void rlyeh_remove_data_arr_clean(rlyeh_remove_data_t* images, size_t len);
rlyeh_remove_data_t* rlyeh_remove_data_arr_allocate(size_t len);

void rlyeh_remove_images_from_images_list(rlyeh_images_list_t* images_list, rlyeh_remove_data_t* imagestokeep, size_t imagestokeeplen);

int rlyeh_remove(const rlyeh_remove_data_t* path_parse);
int rlyeh_remove_unlisted_blobs(const char* storagepath, const char* imagepath);
int rlyeh_remove_unlisted_images(const char* images_path,
                                 rlyeh_remove_data_t* imagestokeep,
                                 size_t imagestokeeplen);



#ifdef __cplusplus
}
#endif

#endif // __RLYEH_REMOVE_H__
